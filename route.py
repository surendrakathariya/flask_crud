from crypt import methods
from datetime import timedelta
from functools import wraps
import hashlib
from textwrap import wrap
from flask import Flask, render_template, redirect, flash
from flask import request, session
from models.model import Todo, User

from web_app import app, db

# signup, Method = GET
@app.route("/signup/")
def sign_up_get():
    if request.method == "GET":
        return render_template("pages/auth/signup.html", action="/signup")


# signup, Method = POST
@app.route("/signup", methods=["POST"])
def sign_up_post():
    username = request.form["username"]
    email = request.form["email"]
    password = request.form["password"]

    data = {
        "username": username,
        "email": email,
        "password": hashlib.sha256(password.encode("utf-8")).hexdigest(),
    }
    user = User(**data)

    db.session.add(user)
    db.session.commit()
    flash("Sign up Successfully")
    return redirect("/login")


# session timeout
@app.before_request
def make_session_permanent():
    session.permanent = True
    app.permanent_session_lifetime = timedelta(minutes=20)


# login_required
def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if "current_user" in session:
            return f(*args, **kwargs)
        else:
            flash("Login first")
            return redirect("/login")

    return wrap


# login method = GET
@app.route("/login")
def user_get():
    return render_template("pages/auth/login.html")


# login, method=POST
@app.route("/login", methods=["POST"])
def login():
    if request.method == "POST":
        email = request.form["email"]
        password = request.form["password"]
        hashed_password = hashlib.sha256(password.encode("utf-8")).hexdigest()
        user = User.query.filter_by(email=email).first()
        if user and user.password == hashed_password:
            session["current_user"] = {
                "username": user.username,
                "client_id": user.client_id,
            }
            flash("Login Successfully")
            return redirect("/list")
        else:
            flash("Invalid Email or Password")
            return redirect("/login")


# logout
@app.route("/logout")
@login_required
def logout():
    session.clear()
    flash("Logout Successfully")
    return redirect("/login")


# Create data, method = GET
@app.route("/add")
@login_required
def create_get():

    return render_template("pages/home.html", action="/add")


# Create data, method = POST
@app.route("/add", methods=["POST"])
def create_post():
    title = request.form["title"]
    description = request.form["description"]
    user_id = request.form["user_id"]
    status = request.form["status"]

    data = {
        "title": title,
        "description": description,
        "user_id": user_id,
        "status": status,
    }
    create_todo = Todo(**data)

    db.session.add(create_todo)
    db.session.commit()
    flash("Successfully added")
    return redirect("/list")


# Retrive Data
@app.route("/list")
@login_required
def list():
    todos = Todo.query.all()
    return render_template(
        "pages/list.html", todos=todos, current_user=session["current_user"]["username"]
    )


# Delete
@app.route("/delete/<int:id>")
@login_required
def delete(id):
    todo = Todo.query.get(id)
    db.session.delete(todo)
    db.session.commit()
    flash("Successfully deleted")
    return redirect("/list")


# Update, method=GET
@app.route("/update/<int:id>")
@login_required
def todo_update_get(id):
    todo = Todo.query.get(id)
    return render_template("pages/update.html", todo=todo, action="/update")


# update, method=POST
@app.route("/update", methods=["POST"])
@login_required
def todo_update_post():
    todo_id = request.form["todo_id"]
    title = request.form["title"]
    description = request.form["description"]
    user_id = request.form["user_id"]
    status = request.form["status"]

    data = {
        "title": title,
        "description": description,
        "user_id": user_id,
        "status": status,
    }
    db.session.query(Todo).filter_by(todo_id=todo_id).update(data)
    db.session.commit()
    flash("Successfully updated")
    return redirect("/list")
