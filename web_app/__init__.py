from flask import Flask
from flask_sqlalchemy import SQLAlchemy

import config

app = Flask(
    __name__,
    static_url_path="",
    static_folder="../web_app/static/",
    template_folder="../web_app/templates/",
)
app.secret_key = "abc"
app.config["SQLALCHEMY_DATABASE_URI"] = config.DATABASE_URI
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db = SQLAlchemy(app)
db.init_app(app)
