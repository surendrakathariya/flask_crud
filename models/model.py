from enum import unique
from web_app import db
from datetime import datetime
from sqlalchemy import Column, Integer, String


#class for users table
class User(db.Model):

    __tablename__ = "users"

    client_id           = Column(Integer, primary_key=True)
    username            = Column(String(100))
    email               = Column(String(100), unique=True)
    password            = Column(String(100))
    created_date        = Column(String(30), default = datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3])


class Todo(db.Model):
    
    __tablename__ = 'Todo'
    
    todo_id  = Column(Integer, primary_key=True)
    title    = Column(String(100))
    description = Column(String(100))
    user_id  = Column(Integer)
    # user_id  = db.Column(db.Integer, db.ForeignKey("users.client_id"),nullable=False)
    
    status = Column(String(100))
    created_date=Column(String(30), default = datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3])

    # def __str__(self):
    #     return self.title
    
    
    










